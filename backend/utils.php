<?php
function reply($status, $response = null, $message = null) {
    $output = array(
        "status" => $status,
        "message" => $message,
        "response" => $response
    );
    echo json_encode($output, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK |
        JSON_UNESCAPED_SLASHES);
    exit;
}
function authorize(Medoo\Medoo $db) {
    $driver = $db->select("drivers", ["id"], [
        "imei" => $_POST['imei']
    ]);
    if (empty($driver)) {
        reply(ERROR, null, 'Устройство не привязано к системе');
    } else {
        return $driver[0]["id"];
    }
    return 0;
}
function sendNotification(Medoo\Medoo $db, $driver) {
    $tokens = $db->select("drivers", ["gcm"], [
        "AND" => [
            "id" => $driver,
            "gcm[!]" => null
        ]
    ]);
    if (count($tokens) === 0) {
        return;
    }
    $fields = array(
        'registration_ids' => [$tokens[0]["gcm"]],
        'data' => [
            'message' => null
        ]
    );
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true)['results'];
    if (isset($data[0]['error']) || isset($data[0]['registration_id']))  {
        $db->update("drivers", [
            "gcm" => null
        ], [
            "id" => $driver
        ]);
    }
    return $result;
}