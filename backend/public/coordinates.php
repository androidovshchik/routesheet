<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_POST);
$validator->mapFieldsRules([
    'imei' => ['required', ['length', 15]],
    'lat' => ['required'],
    'lng' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../setup.json"), true));

$driver = authorize($db);

$db->delete("coordinates", [
    "AND" => [
        "driver" => $driver,
        "time[<]" => date("Y-m-d H:i:s", time() - 3 * 24 * 60 * 60)
    ]
]);

$db->update("coordinates", [
    "distance" => 0
], [
    "driver" => $driver,
    "LIMIT" => 1
]);

$previous = $db->select("coordinates", ["lat", "lng"], [
    "driver" => $driver,
    "LIMIT" => 1,
    "ORDER" => [
        "id" => "DESC"
    ]
]);

$lat2 = $_POST['lat'];
$lng2 = $_POST['lng'];
$distance = 0;
if (count($previous) != 0) {
    $lat1 = $previous[0]['lat'];
    $lng1 = $previous[0]['lng'];
    $distance = 3958 * 3.1415926 * sqrt(($lat2 - $lat1) * ($lat2 - $lat1) +
            cos($lat2 / 57.29578) * cos($lat1 / 57.29578) * ($lng2 - $lng1) * ($lng2 - $lng1)) / 180 / 0.00062137;
    if ($distance < 60) {
        reply(WARNING, null, null);
    }
}

$db->update("drivers", [
    "lat" => $lat2,
    "lng" => $lng2
], [
    "id" => $driver
]);

$db->insert("coordinates", [
    "driver" => $driver,
    "lat" => $lat2,
    "lng" => $lng2,
    "distance" => $distance
]);

reply(SUCCESS, null, null);
