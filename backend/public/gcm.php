<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_POST);
$validator->mapFieldsRules([
    'imei' => ['required', ['length', 15]],
    'token' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../setup.json"), true));

$driver = authorize($db);

$db->update("drivers", [
    "gcm" => $_POST['token']
], [
    "id" => $driver
]);

reply(SUCCESS, null, null);
