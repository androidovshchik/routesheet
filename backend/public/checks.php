<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_POST);
$validator->mapFieldsRules([
    'id' => ['required'],
    'bool' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../setup.json"), true));

$db->update("tasks", [
    "checked" => $_POST['bool']
], [
    "id" => $_POST['id']
]);

reply(SUCCESS, null, null);
