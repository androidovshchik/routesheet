<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_GET);
$validator->mapFieldsRules([
    'name' => ['required', ['lengthMax', 70]],
    'imei' => ['required', ['length', 15]]
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../../setup.json"), true));

$result = $db->insert("drivers", [
    "name" => $_GET['name'],
    "imei" => $_GET['imei']
]);

if ($result->rowCount() == 1) {
    reply(SUCCESS, null, "Водитель успешно добавлен");
} else {
    reply(ERROR, null, "Водитель с данным IMEI уже существует");
}
