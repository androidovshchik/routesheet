<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_GET);
$validator->mapFieldsRules([
    'ids' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$ids = json_decode($_GET['ids']);
if (count($ids) == 0) {
    reply(WARNING, null, "Что-то пошло не так");
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../../setup.json"), true));

$driver = $db->select("tasks", ["driver"], [
    "id" => $ids[0]
])[0]["driver"];

$result = $db->delete("tasks", [
    "id" => $ids
]);

if ($result->rowCount() == count($ids)) {
    if (count($ids) == 1) {
        sendNotification($db, $driver);
        reply(SUCCESS, null, "Заданиe успешно удалено");
    } else {
        sendNotification($db, $driver);
        reply(SUCCESS, null, "Задания успешно удалены");
    }
} else if ($result->rowCount() > 0) {
    sendNotification($db, $driver);
    reply(WARNING, null, "Часть заданий не удалено");
} else {
    if (count($ids) == 1) {
        reply(ERROR, null, "Не удалось удалить задание");
    } else {
        reply(ERROR, null, "Не удалось удалить задания");
    }
}