<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_POST);
$validator->mapFieldsRules([
    'driver' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$csv = file_get_contents($_FILES['csv']['tmp_name']);
if (!preg_match('//u', $csv)) {
    $csv = mb_convert_encoding($csv, 'UTF-8', 'windows-1251');
}
$data = array_map(function($input) {
    return str_getcsv($input, ";");
}, explode("\n", $csv));
array_splice($data, 0, 1);

if (count($data) <= 0) {
    reply(WARNING, null, "Отсутствуют задания");
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../../setup.json"), true));

//0Номер;1Контрагент;2Адрес;3Телефон;4Контактное Лицо;5Менеджер;6Форма оплаты;7Водитель;8Задание
$rows = 0;
foreach ($data as $array) {
    if (count($array) == 1 && is_null($array[0])) {
        $rows += 1;
        continue;
    }
    if (count($array) != 9) {
        reply(ERROR, $array, "Ошибка при обработке файла");
    }
    $rows += $db->insert("tasks", [
        "driver" => $_POST['driver'],
        "name" => $array[1],
        "address" => $array[2],
        "phone" => $array[3],
        "contactName" => $array[4],
        "payment" => $array[6],
        "task" => $array[8],
        "manager" => $array[5]
    ])->rowCount();
}

if ($rows == count($data)) {
    sendNotification($db, $_POST['driver']);
    reply(SUCCESS, null, "Задания успешно добавлены");
} else if ($rows > 0) {
    sendNotification($db, $_POST['driver']);
    reply(WARNING, null, "Часть заданий не добавлено");
} else {
    reply(ERROR, null, "Не удалось добавить задания");
}

