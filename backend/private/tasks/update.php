<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_GET);
$validator->mapFieldsRules([
    'id' => ['required'],
    'name' => ['required'],
    'address' => ['required'],
    'phone' => ['required'],
    'contactName' => ['required'],
    'payment' => ['required'],
    'task' => ['required'],
    'manager' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../../setup.json"), true));

$db->update("tasks", [
    "name" => $_GET['name'],
    "address" => $_GET['address'],
    "phone" => $_GET['phone'],
    "contactName" => $_GET['contactName'],
    "payment" => $_GET['payment'],
    "task" => $_GET['task'],
    "manager" => $_GET['manager'],
    "lat" => null,
    "lng" => null
], [
    "id" => $_GET['id']
]);

$result = sendNotification($db, $db->select("tasks", ["driver"], [
    "id" => $_GET['id']
])[0]["driver"]);

reply(SUCCESS, $result, "Задание успешно изменено");