<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require_once __DIR__ . "/../../vendor/autoload.php";

use Medoo\Medoo;
use Valitron\Validator;

$validator = new Validator($_GET);
$validator->mapFieldsRules([
    'driver' => ['required']
]);
if (!$validator->validate()) {
    reply(ERROR, null, 'Невалидные данные');
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../../setup.json"), true));

reply(SUCCESS, $db->select("tasks", "*", [
    "driver" => $_GET['driver']
]), null);