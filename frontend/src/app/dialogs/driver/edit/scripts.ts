import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-dialog-driver-edit',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogDriverEditComponent {

    constructor(public dialogRef: MatDialogRef<DialogDriverEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
