import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-dialog-driver-add',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogDriverAddComponent {

    constructor(public dialogRef: MatDialogRef<DialogDriverAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
