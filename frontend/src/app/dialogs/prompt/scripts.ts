import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-dialog-prompt',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogPromptComponent {

    constructor(public dialogRef: MatDialogRef<DialogPromptComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
