import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ApiService} from '../../services/api';

@Component({
    selector: 'app-dialog-upload',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class DialogUploadComponent {

    file: File = null;

    constructor(private api: ApiService, public dialogRef: MatDialogRef<DialogUploadComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {}

    fileChange(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.file = fileList[0];
        }
    }

    onUpload(): void {
        if (this.file == null) {
            return;
        }
        this.api.uploadTasks(this.file, this.data.driver).subscribe((data) => {
            console.log(data);
            if (data.status == 0) {
                this.dialogRef.close(data.message);
            } else {
                this.dialogRef.close();
            }
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
