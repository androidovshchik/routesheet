import {Component, ViewChild} from '@angular/core';
import {DialogDriverAddComponent} from './dialogs/driver/add/scripts';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ApiService} from './services/api';
import {TasksComponent} from './components/tasks/scripts';

declare const google: any;

@Component({
    selector: 'app-root',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class RootComponent {

    @ViewChild(TasksComponent)
    tasks: TasksComponent;

    map: any;

    moscow = {
        lat: 55.75,
        lng: 37.61,
        zoom: 10
    };

    constructor(private api: ApiService, public dialog: MatDialog, public snackBar: MatSnackBar) {}

    public loadAPIWrapper(map) {
        this.map = map;
    }

    onScale() {
        this.map.setCenter({
            lat: 55.75,
            lng: 37.61
        });
        this.animateMapZoomTo(10);
    }

    animateMapZoomTo(targetZoom) {
        let currentZoom = this.map.getZoom();
        if (currentZoom != targetZoom) {
            google.maps.event.addListenerOnce(this.map, 'zoom_changed', () => {
                this.animateMapZoomTo(targetZoom);
            });
            setTimeout(() => {
                this.map.setZoom(currentZoom + (targetZoom > currentZoom ? 1 : -1))
            }, 80);
        }
    }

    addDriver() {
        let dialogRef = this.dialog.open(DialogDriverAddComponent, {
            width: '280px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            this.api.addDriver(result.name, result.imei).subscribe((data) => {
                console.log(data);
                this.snackBar.open(data.message, null, {
                    duration: 1500,
                });
            });
        });
    }
}
