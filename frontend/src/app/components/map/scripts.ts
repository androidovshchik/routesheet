import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GoogleMapsAPIWrapper} from '@agm/core';

@Component({
    selector: 'core-map-content',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class MapContentComponent implements OnInit {

    @Output()
    onMapLoad: EventEmitter<{}> = new EventEmitter<{}>();

    constructor(public googleMaps: GoogleMapsAPIWrapper) {}

    ngOnInit() {
        this.googleMaps.getNativeMap().then((map) => {
            this.onMapLoad.emit(map);
        });
    }
}
