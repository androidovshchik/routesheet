import {Component, EventEmitter, NgModule, OnDestroy, OnInit, Output} from '@angular/core';
import {
    MatButtonModule,
    MatCardModule, MatDialog, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatSelectModule, MatSnackBar, MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {CommonModule, DatePipe} from '@angular/common';
import {ApiService} from '../../services/api';
import {Subscription} from 'rxjs/Subscription';
import {TimerObservable} from 'rxjs/observable/TimerObservable';
import {DialogPromptComponent} from '../../dialogs/prompt/scripts';
import {DialogUploadComponent} from '../../dialogs/upload/scripts';
import {DialogDriverEditComponent} from '../../dialogs/driver/edit/scripts';
import {GoogleMapsService} from '../../services/map';

@Component({
    selector: 'app-tasks',
    templateUrl: './index.html',
    styleUrls: ['./styles.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

    private timerSubscription: Subscription;

    driver: number = 0;

    drivers = [];

    tasks = [];

    coordinates = [];

    days = [
        {
            "name": "Сегодня",
            "date": "",
            "distance": 0,
            "finish": 0,
            "tasks": []
        },
        {
            "name": "Вчера",
            "date": "",
            "distance": 0,
            "finish": 0,
            "tasks": []
        },
        {
            "name": "Позавчера",
            "date": "",
            "distance": 0,
            "finish": 0,
            "tasks": []
        },
        {
            "name": "Позапозавчера",
            "date": "",
            "distance": 0,
            "finish": 0,
            "tasks": []
        }
    ];

    @Output()
    scaleEvent = new EventEmitter<void>();

    constructor(private googleMapsService: GoogleMapsService, private api: ApiService, public dialog: MatDialog,
                private snackBar: MatSnackBar, private datePipe: DatePipe) {}

    ngOnInit() {
        let timer = TimerObservable.create(0, 10000);
        this.timerSubscription = timer.subscribe(() => {
            this.getDrivers();
            this.getCoordinates();
        });
    }

    getDrivers() {
        let date = new Date();
        this.days.forEach((item) => {
            item.date = this.datePipe.transform(date, 'yyyy-MM-dd');
            date.setDate(date.getDate() - 1);
        });
        this.api.getAllDrivers().subscribe((data) => {
            console.log(data);
            if (data.status == 0) {
                this.drivers = data.response;
            }
        });
    }

    getCoordinates() {
        if (this.driver == 0) {
            return;
        }
        this.api.getCoordinates(this.driver).subscribe((data) => {
            if (data.status == 0) {
                this.coordinates = data.response;
                this.days.forEach((item) => {
                    item.distance = 0;
                    item.finish = 0;
                    item.tasks.splice(0, item.tasks.length);
                    item.tasks.push({
                        name: 'На начало дня',
                        distance: this.beautifyDistance(0)
                    });
                });
                let distance = 0;
                let day = -1;
                const tasks = JSON.parse(JSON.stringify(this.tasks));
                for (let c = 0; c < this.coordinates.length; c++) {
                    let itemDistance = Math.ceil(this.coordinates[c].distance);
                    for (let d = 0; d < this.days.length; d++) {
                        if (this.coordinates[c].time.startsWith(this.days[d].date)) {
                            if (day != -1) {
                                if (day != d) {
                                    this.days[day].finish = distance;
                                    distance = 0;
                                    day = -1;
                                } else {
                                    distance += Math.ceil(this.coordinates[c].distance);
                                }
                            }
                            if (day == -1) {
                                day = d;
                                itemDistance = 0;
                            }
                            this.days[day].distance += itemDistance;
                            for (let t = 0; t < tasks.length; t++) {
                                if (tasks[t].lat == null || tasks[t].lng == null) {
                                    continue;
                                }
                                if (Math.abs(tasks[t].lat - this.coordinates[c].lat) < 0.001 &&
                                    Math.abs(tasks[t].lng - this.coordinates[c].lng) < 0.001) {
                                    this.days[day].tasks.push({
                                        name: 'Задание №' + (t + 1),
                                        distance: this.beautifyDistance(distance)
                                    });
                                    distance = 0;
                                    tasks[t].lat = tasks[t].lng = null;
                                }
                            }
                            break;
                        }
                    }
                    if (c == this.coordinates.length - 1) {
                        this.days[day].finish = distance;
                    }
                }
                this.days.forEach((item) => {
                    item.tasks.push({
                        name: 'На конец дня',
                        distance: this.beautifyDistance(item.finish)
                    });
                });
            }
        });
    }

    onScale() {
        this.scaleEvent.next();
    }

    beautifyDistance(distance: any) {
        if (distance > 1009) {
            return (distance / 1000).toFixed(2) + " км.";
        } else {
            return distance + " м.";
        }
    }

    onDriver(id: number) {
        this.coordinates.splice(0, this.coordinates.length);
        this.days.forEach((item) => {
            item.distance = 0;
            item.tasks.splice(0, item.tasks.length);
        });
        this.api.getAllTasks(id).subscribe((data) => {
            console.log(data);
            if (data.status == 0) {
                this.tasks = data.response;
                this.tasks.forEach((item) => {
                    if (item.lat == null || item.lng == null) {
                        this.googleMapsService.toLatLan(item)
                            .subscribe();
                    }
                });
            }
        });
        this.getDrivers();
        this.getCoordinates();
    }

    editTask(position: number) {
        const task = Object.assign({}, this.tasks[position]);
        let dialogRef = this.dialog.open(DialogDriverEditComponent, {
            width: '280px',
            data: {
                position: position,
                task: this.tasks[position]
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                this.tasks[position] = task;
                return;
            }
            this.api.updateTask(result).subscribe((data) => {
                console.log(data);
                this.showMessage(data.message);
                this.googleMapsService.toLatLan(result)
                    .subscribe();
            });
        });
    }

    uploadTasks() {
        let dialogRef = this.dialog.open(DialogUploadComponent, {
            width: '320px',
            data: {
                driver: this.driver
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            this.showMessage(result);
            this.onDriver(this.driver);
        });
    }

    deleteTask(position: number) {
        let dialogRef = this.dialog.open(DialogPromptComponent, {
            width: '240px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            let array = [];
            array.push(this.tasks[position].id);
            this.api.deleteTasks(array).subscribe((data) => {
                console.log(data);
                this.showMessage(data.message);
                if (data.status == 0) {
                    this.tasks.splice(position, 1);
                }
            });
        });
    }

    deleteTasks() {
        let dialogRef = this.dialog.open(DialogPromptComponent, {
            width: '240px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            let array = [];
            this.tasks.forEach((item) => {
                array.push(item.id);
            });
            this.api.deleteTasks(array).subscribe((data) => {
                console.log(data);
                this.showMessage(data.message);
                if (data.status == 0) {
                    this.tasks.splice(0, this.tasks.length);
                }
            });
        });
    }

    deleteDriver() {
        let dialogRef = this.dialog.open(DialogPromptComponent, {
            width: '240px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result == null) {
                return;
            }
            this.api.deleteDriver(this.driver).subscribe((data) => {
                console.log(data);
                this.showMessage(data.message);
                if (data.status == 0) {
                    for (let i = this.drivers.length - 1; i >= 0; i--) {
                        if (this.driver == this.drivers[i].id) {
                            this.drivers.splice(i, 1);
                            break;
                        }
                    }
                    this.driver = 0;
                    this.tasks = [];
                    this.coordinates = [];
                }
            });
        });
    }

    private showMessage(message: string) {
        if (message == null) {
            return;
        }
        this.snackBar.open(message, null, {
            duration: 1500,
        });
    }

    ngOnDestroy() {
        this.timerSubscription.unsubscribe();
    }
}

@NgModule({
    imports: [
        MatToolbarModule,
        MatFormFieldModule,
        MatSelectModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatSnackBarModule,
        FormsModule,
        CommonModule
    ],
    exports: [TasksComponent],
    entryComponents: [
        DialogPromptComponent,
        DialogUploadComponent,
        DialogDriverEditComponent
    ],
    declarations: [
        TasksComponent,
        DialogPromptComponent,
        DialogUploadComponent,
        DialogDriverEditComponent
    ],
    providers: [
        DatePipe
    ]
})
export class TasksModule {}
