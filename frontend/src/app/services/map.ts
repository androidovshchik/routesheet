import {Injectable, NgZone} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GoogleMapsAPIWrapper, MapsAPILoader} from '@agm/core';
import {ApiService} from './api';

declare const google: any;

@Injectable()
export class GoogleMapsService extends GoogleMapsAPIWrapper {

    constructor(private __loader: MapsAPILoader, private __zone: NgZone, private api: ApiService) {
        super(__loader, __zone);
    }

    toLatLan(item: any) {
        let geoCoder = new google.maps.Geocoder();
        return Observable.create(observer => {
            geoCoder.geocode({'address': item.address},
                (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        item.lat = results[0].geometry.location.lat();
                        item.lng = results[0].geometry.location.lng();
                        this.api.setLatLng(item).subscribe((data) => {
                            console.log(data);
                        });
                    }
                });
            observer.complete();
        }).catch(GoogleMapsService.handleError);
    }

    private static handleError(error:any) {
        console.error(error);
        return Observable.throw(error);
    }
}