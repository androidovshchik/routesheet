import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

    url: string = 'http://dentalclub.shop/tracker/api/private/';

    constructor (private http: Http) {}

    getAllDrivers() {
        return this.http.get(this.url + 'drivers/getAll.php',
            null).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    addDriver(name: string, imei: string) {
        return this.http.get(this.url + 'drivers/add.php',
            new RequestOptions({
                params: {
                    name: name,
                    imei: imei
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    getAllTasks(driver: number) {
        return this.http.get(this.url + 'tasks/getAll.php',
            new RequestOptions({
                params: {
                    driver: driver
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    getCoordinates(driver: number) {
        return this.http.get(this.url + 'coordinates.php',
            new RequestOptions({
                params: {
                    driver: driver
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    updateTask(task: any) {
        return this.http.get(this.url + 'tasks/update.php',
            new RequestOptions({
                params: {
                    id: task.id,
                    name: task.name,
                    address: task.address,
                    phone: task.phone,
                    contactName: task.contactName,
                    payment: task.payment,
                    task: task.task,
                    manager: task.manager
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    deleteTasks(tasks) {
        return this.http.get(this.url + 'tasks/delete.php',
            new RequestOptions({
                params: {
                    ids: JSON.stringify(tasks)
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    deleteDriver(driver: number) {
        return this.http.get(this.url + 'drivers/delete.php',
            new RequestOptions({
                params: {
                    driver: driver
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    uploadTasks(file: File, driver) {
        let formData:FormData = new FormData();
        formData.append('csv', file, file.name);
        formData.append('driver', driver);
        return this.http.post(this.url + 'tasks/add.php', formData, null)
            .map(res => res.json())
            .catch(ApiService.handleError).share();
    }

    setLatLng(item: any) {
        return this.http.get(this.url + 'latlng.php',
            new RequestOptions({
                params: {
                    id: item.id,
                    lat: item.lat,
                    lng: item.lng
                }
            })).map((res: any) => res.json()).catch(ApiService.handleError).share();
    }

    private static handleError(error:any) {
        console.error(error);
        return Observable.throw(error);
    }
}
