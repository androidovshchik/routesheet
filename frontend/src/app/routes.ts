import {Routes} from '@angular/router';
import {RootComponent} from './scripts';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: RootComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: ''
  },
];
