import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RootComponent} from './scripts';
import {APP_ROUTES} from './routes';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core';
import {TasksModule} from './components/tasks/scripts';
import {FormsModule} from '@angular/forms';
import {
    MatButtonModule, MatDialogModule, MatIconModule, MatInputModule, MatSidenavModule,
    MatSnackBarModule
} from '@angular/material';
import {ApiService} from './services/api';
import {HttpModule} from '@angular/http';
import {DialogDriverAddComponent} from './dialogs/driver/add/scripts';
import {MapContentComponent} from './components/map/scripts';
import {GoogleMapsService} from './services/map';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        MatSidenavModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatInputModule,
        MatSnackBarModule,
        TasksModule,
        RouterModule.forRoot(APP_ROUTES),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBLf9WYd6rXRlH97-LRGphbw_09eLfPtq0'
        })
    ],
    declarations: [
        RootComponent,
        DialogDriverAddComponent,
        MapContentComponent
    ],
    providers: [
        ApiService,
        GoogleMapsService,
        GoogleMapsAPIWrapper,
        {
            provide: LocationStrategy, useClass: PathLocationStrategy
        }
    ],
    entryComponents: [
        DialogDriverAddComponent
    ],
    bootstrap: [
        RootComponent
    ]
})
export class AppModule {}
