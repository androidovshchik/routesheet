package rf.androidovshchik.routesheet;

public interface Constants {

    String ACTION_STOP = "stop";
    String ACTION_RESTART = "restart";

    int DELAY_LOCATION_MILLIS = 10000;

    String EXTRA_TASKS = "tasks";
    String EXTRA_POSITION = "position";
}
