package rf.androidovshchik.routesheet.ui.base;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import java.util.ArrayList;

public abstract class BaseAdapter<T, V extends BaseViewHolder> extends RecyclerView.Adapter<V> {

    public ArrayList<T> items;

    public BaseAdapter() {
        items = new ArrayList<>();
    }

    public void setItems(ArrayList<T> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewRecycled(V holder) {
        super.onViewRecycled(holder);
        if (holder.itemView instanceof ImageView) {
            ((ImageView) holder.itemView).setImageDrawable(null);
        }
    }
}
