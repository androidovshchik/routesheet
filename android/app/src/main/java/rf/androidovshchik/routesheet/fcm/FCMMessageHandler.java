package rf.androidovshchik.routesheet.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.events.NotificationEvent;
import rf.androidovshchik.routesheet.ui.main.MainActivity;
import rf.androidovshchik.routesheet.utils.EventUtil;

public class FCMMessageHandler extends FirebaseMessagingService {

	public static final int MESSAGE_NOTIFICATION_ID = 435345;

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		EventUtil.post(new NotificationEvent());
		NotificationManager notificationManager = (NotificationManager)
			getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
				getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
			notificationManager.createNotificationChannel(channel);
		}
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(R.drawable.ic_directions_car_white_24dp)
			.setContentText(getString(R.string.title_notification))
			.setSound(soundUri)
			.setAutoCancel(true);
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		builder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent, 0));
		notificationManager.notify(MESSAGE_NOTIFICATION_ID, builder.build());
	}
}