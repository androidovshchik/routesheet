package rf.androidovshchik.routesheet.services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;

import com.yayandroid.locationmanager.base.LocationBaseService;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.constants.FailType;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.routesheet.Constants;
import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.RouteSheet;
import rf.androidovshchik.routesheet.events.CoordinatesEvent;
import rf.androidovshchik.routesheet.receivers.CoordinatesTrigger;
import rf.androidovshchik.routesheet.ui.main.MainActivity;
import rf.androidovshchik.routesheet.utils.ComponentUtil;
import rf.androidovshchik.routesheet.utils.EventUtil;
import rf.androidovshchik.routesheet.utils.LocationUtil;
import timber.log.Timber;

public class CoordinatesService extends LocationBaseService {

	private CompositeDisposable disposable;

	private ScheduledFuture<?> locationChecker;

	private PowerManager.WakeLock wakeLock;

	private AlarmManager alarmManager;

	private TelephonyManager telephonyManager;

	private Float lat;

	private Float lng;

	private Long lastUpdate;

	@Override
	public LocationConfiguration getLocationConfiguration() {
		return LocationUtil.getConfiguration();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static Intent getStartIntent(Context context) {
		return new Intent(context, CoordinatesService.class);
	}

	public static boolean isRunning(Context context) {
		return ComponentUtil.isServiceRunning(context, CoordinatesService.class);
	}

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WAKE_LOCK");
		wakeLock.acquire();
		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		disposable = new CompositeDisposable();
		setAlarm();
		startForeground();
	}

	@Override
	@SuppressWarnings("all")
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		getLocation();
		ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
		locationChecker = timer.scheduleWithFixedDelay(() -> {
			Timber.d("Checking location...");
			if (lastUpdate != null && Math.abs(System.currentTimeMillis() - lastUpdate) > 30000) {
				lastUpdate = null;
				lat = lng = null;
			}
			if (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION) &&
				hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) &&
				hasPermission(Manifest.permission.READ_PHONE_STATE) &&
				lat != null && lng != null) {
				Timber.d("lat %f lng %f", lat, lng);
				disposable.add(RouteSheet.client.api
					.setCoordinates(telephonyManager.getDeviceId(), lat, lng)
					.subscribe((response) -> {}, throwable -> Timber.e(throwable)));
			}
		}, 0, Constants.DELAY_LOCATION_MILLIS, TimeUnit.MILLISECONDS);
		return START_NOT_STICKY;
	}

	private void startForeground() {
		NotificationManager notificationManager = (NotificationManager)
			getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
				getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
			notificationManager.createNotificationChannel(channel);
		}
		Intent activity = new Intent(getApplicationContext(), MainActivity.class);
		activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Intent receiver = new Intent(getApplicationContext(), CoordinatesTrigger.class);
		receiver.setAction(Constants.ACTION_STOP);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(R.drawable.ic_gps_fixed_white_24dp)
			.setContentTitle(getString(R.string.title_location))
			.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, activity,
				PendingIntent.FLAG_UPDATE_CURRENT))
			.setPriority(Notification.PRIORITY_MAX)
			.addAction(0, getString(R.string.action_stop),
				PendingIntent.getBroadcast(getApplicationContext(), 0, receiver,
					PendingIntent.FLAG_UPDATE_CURRENT));
		startForeground(1, builder.build());
	}

	private void setAlarm() {
		Intent receiver = new Intent(getApplicationContext(), CoordinatesTrigger.class);
		receiver.setAction(Constants.ACTION_RESTART);
		PendingIntent pendingReceiver = PendingIntent.getBroadcast(getApplicationContext(),
			0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.cancel(pendingReceiver);
		alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
			SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HOUR,
			AlarmManager.INTERVAL_HOUR, pendingReceiver);
	}

	private boolean hasPermission(String permission) {
		return checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public void onLocationChanged(Location location) {
		lastUpdate = System.currentTimeMillis();
		lat = (float) location.getLatitude();
		lng = (float) location.getLongitude();
		EventUtil.post(new CoordinatesEvent(lat, lng));
	}

	@Override
	public void onLocationFailed(@FailType int type) {}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (locationChecker != null) {
			locationChecker.cancel(true);
		}
		disposable.dispose();
		wakeLock.release();
		stopForeground(true);
		stopSelf();
	}
}