package rf.androidovshchik.routesheet.fcm;

import com.google.firebase.iid.FirebaseInstanceIdService;

import rf.androidovshchik.routesheet.RouteSheet;

public class InstanceIDListenerService  extends FirebaseInstanceIdService {

	@Override
	public void onTokenRefresh() {
		RouteSheet.sendRegistrationIdToBackend(getApplicationContext());
	}
}
