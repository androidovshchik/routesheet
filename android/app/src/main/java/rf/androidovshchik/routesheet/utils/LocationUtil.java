package rf.androidovshchik.routesheet.utils;

import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.Nullable;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration;
import com.yayandroid.locationmanager.configuration.GooglePlayServicesConfiguration;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.constants.ProviderType;

import java.io.IOException;
import java.util.List;

import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.routesheet.Constants;
import rf.androidovshchik.routesheet.RouteSheet;
import rf.androidovshchik.routesheet.models.Task;
import timber.log.Timber;

public final class LocationUtil {

    @SuppressWarnings("unused")
    public static LocationConfiguration getConfiguration() {
        return new LocationConfiguration.Builder()
            .keepTracking(true)
            .useGooglePlayServices(new GooglePlayServicesConfiguration.Builder()
                .locationRequest(LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(Constants.DELAY_LOCATION_MILLIS))
                .fallbackToDefault(true)
                .setWaitPeriod(0)
                .build())
            .useDefaultProviders(new DefaultProviderConfiguration.Builder()
                .requiredTimeInterval(Constants.DELAY_LOCATION_MILLIS)
                .setWaitPeriod(ProviderType.GPS, 0)
                .setWaitPeriod(ProviderType.NETWORK, 0)
                .build())
            .build();
    }

    @Nullable
    @SuppressWarnings("all")
    public static LatLng createLatLng(Task task, Geocoder geocoder, boolean send2Server) {
        if (task.lat != null && task.lng != null) {
            return new LatLng(task.lat, task.lng);
        }
        try {
            List<Address> addresses = geocoder.getFromLocationName(task.address, 1);
            if (addresses.size() > 0) {
                if (send2Server) {
                    RouteSheet.client.api.setLatLng(task.id, (float) addresses.get(0).getLatitude(),
                        (float) addresses.get(0).getLongitude())
                        .subscribeOn(Schedulers.io())
                        .subscribe((response) -> {}, throwable -> Timber.e(throwable));
                }
                return new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        } catch (IOException e) {
            Timber.e(e);
        }
        return null;
    }

    @SuppressWarnings("unused")
    @Nullable
    public static MarkerOptions createMarkerOptions(IconGenerator generator,
                                                    @Nullable LatLng position, String text,
                                                    @Nullable String color) {
        if (position == null) {
            return null;
        }
        if (color != null) {
            generator.setStyle(IconGenerator.STYLE_BLUE);
            generator.setColor(Color.parseColor(color.replace("#ff", "#b3")));
        } else {
            generator.setStyle(IconGenerator.STYLE_DEFAULT);
            generator.setColor(Color.parseColor("#b3ffffff"));
        }
        return new MarkerOptions()
            .icon(BitmapDescriptorFactory.fromBitmap(generator.makeIcon(text)))
            .position(position)
            .anchor(generator.getAnchorU(), generator.getAnchorV());
    }
}
