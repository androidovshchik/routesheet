package rf.androidovshchik.routesheet.remote;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rf.androidovshchik.routesheet.models.Coordinates;
import rf.androidovshchik.routesheet.models.Response;
import rf.androidovshchik.routesheet.models.Task;

public interface Api {

    String ENDPOINT = "http://dentalclub.shop/tracker/api/public/";

    @FormUrlEncoded
    @POST("tasks.php")
    Observable<Response<Task>> getTasks(@Field("imei") String imei);

    @FormUrlEncoded
    @POST("coordinates.php")
    Observable<Response<Void>> setCoordinates(@Field("imei") String imei, @Field("lat") float lat,
                                    @Field("lng") float lng);

    @FormUrlEncoded
    @POST("track.php")
    Observable<Response<Coordinates>> getCoordinates(@Field("imei") String imei);

    @FormUrlEncoded
    @POST("latlng.php")
    Observable<Response<Void>> setLatLng(@Field("id") long taskId, @Field("lat") float lat,
                                         @Field("lng") float lng);

    @FormUrlEncoded
    @POST("gcm.php")
    Observable<Response<Void>> setGCMToken(@Field("imei") String imei, @Field("token") String token);

    @FormUrlEncoded
    @POST("checks.php")
    Observable<Response<Void>> checkTask(@Field("id") long taskId, @Field("bool") int bool);
}