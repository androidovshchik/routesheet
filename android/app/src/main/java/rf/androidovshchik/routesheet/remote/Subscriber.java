package rf.androidovshchik.routesheet.remote;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

public abstract class Subscriber<T> extends DisposableObserver<T> {

    @Override
    public void onComplete() {}

    @Override
    public void onError(Throwable e) {
        Timber.e(e);
    }
}