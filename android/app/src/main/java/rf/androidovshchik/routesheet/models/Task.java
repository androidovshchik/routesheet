package rf.androidovshchik.routesheet.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

public class Task implements Parcelable {

    @SerializedName("id")
    public long id;

    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("phone")
    public String phone;

    @SerializedName("contactName")
    public String contactName;

    @SerializedName("payment")
    public String payment;

    @SerializedName("task")
    public String task;

    @SerializedName("manager")
    public String manager;

    @Nullable
    @SerializedName("lat")
    public Float lat;

    @Nullable
    @SerializedName("lng")
    public Float lng;

    @SerializedName("checked")
    public int checked;

    public transient boolean achieved = false;

    public Task() {}

    public Task(String address) {
        this.address = address;
    }

    public Task(boolean empty) {
        this.address = this.name = this.phone = this.contactName = this.payment
            = this.task = this.manager = "";
    }

    public Task(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.address = in.readString();
        this.phone = in.readString();
        this.contactName = in.readString();
        this.payment = in.readString();
        this.task = in.readString();
        this.manager = in.readString();
        this.lat = (Float) in.readValue(Float.class.getClassLoader());
        this.lng = (Float) in.readValue(Float.class.getClassLoader());
        this.checked = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(contactName);
        dest.writeString(payment);
        dest.writeString(task);
        dest.writeString(manager);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeInt(checked);
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {

        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        public Task[] newArray(int size) {
            return new Task[size];
        }
    };
}