package rf.androidovshchik.routesheet.ui.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.routesheet.Constants;
import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.RouteSheet;
import rf.androidovshchik.routesheet.models.Task;
import rf.androidovshchik.routesheet.ui.base.BaseAdapter;
import rf.androidovshchik.routesheet.ui.base.BaseViewHolder;
import rf.androidovshchik.routesheet.ui.map.MapsActivity;
import timber.log.Timber;

public class TasksAdapter extends BaseAdapter<Task, TasksAdapter.TasksViewHolder> {

    public TasksAdapter() {
        super();
    }

    @Override
    public TasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task,
            parent, false);
        return new TasksViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TasksViewHolder holder, int position) {
        holder.index.setText(holder.getString(R.string.caption_index, position + 1,
            items.get(position).achieved ? "[Выполнено]" : ""));
        holder.name.setText(holder.getString(R.string.caption_name, items.get(position).name));
        holder.address.setText(holder.getString(R.string.caption_address,
            items.get(position).address));
        holder.phone.setText(holder.getString(R.string.caption_phone, items.get(position).phone));
        holder.contactName.setText(holder.getString(R.string.caption_contactName,
            items.get(position).contactName));
        holder.payment.setText(holder.getString(R.string.caption_payment,
            items.get(position).payment));
        holder.task.setText(holder.getString(R.string.caption_task, items.get(position).task));
        holder.manager.setText(holder.getString(R.string.caption_manager,
            items.get(position).manager));
        holder.checkBox.setChecked(items.get(position).checked == 1);
    }

    public class TasksViewHolder extends BaseViewHolder {

        @BindView(R.id.index)
        TextView index;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.contactName)
        TextView contactName;
        @BindView(R.id.payment)
        TextView payment;
        @BindView(R.id.task)
        TextView task;
        @BindView(R.id.manager)
        TextView manager;
        @BindView(R.id.check)
        CheckBox checkBox;

        public TasksViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @OnClick(R.id.item)
        void onTask() {
            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.EXTRA_POSITION, getAdapterPosition());
            intent.putExtra(Constants.EXTRA_TASKS, items);
            getApplicationContext().startActivity(intent);
        }

        @OnClick(R.id.check)
        void onChecked() {
            checkBox.setEnabled(false);
            items.get(getAdapterPosition()).checked = checkBox.isChecked() ? 1 : 0;
            RouteSheet.client.api.checkTask(items.get(getAdapterPosition()).id,
                checkBox.isChecked() ? 1 : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((response) -> checkBox.setEnabled(true), throwable -> {
                    Timber.e(throwable);
                    checkBox.setEnabled(true);
                    checkBox.setChecked(!checkBox.isChecked());
                    items.get(getAdapterPosition()).checked = checkBox.isChecked() ? 1 : 0;
                });
        }
    }
}
