package rf.androidovshchik.routesheet.events;

public class CoordinatesEvent {

	public float lat;

	public float lng;

	public CoordinatesEvent(float lat, float lng) {
		this.lat = lat;
		this.lng = lng;
	}
}
