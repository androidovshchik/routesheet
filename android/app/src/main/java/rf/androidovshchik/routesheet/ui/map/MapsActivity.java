package rf.androidovshchik.routesheet.ui.map;

import android.Manifest;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.yayandroid.locationmanager.LocationManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.routesheet.Constants;
import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.events.CoordinatesEvent;
import rf.androidovshchik.routesheet.models.Task;
import rf.androidovshchik.routesheet.remote.Subscriber;
import rf.androidovshchik.routesheet.services.CoordinatesService;
import rf.androidovshchik.routesheet.ui.base.BaseActivity;
import rf.androidovshchik.routesheet.utils.LocationUtil;
import rf.androidovshchik.routesheet.utils.ViewUtil;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.layout)
    ViewGroup layout;
    @BindView(R.id.card)
    ViewGroup card;
    @BindView(R.id.input)
    EditText addressView;
    @BindView(R.id.expand)
    ImageView expand;

    @BindView(R.id.index)
    TextView index;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.contactName)
    TextView contactName;
    @BindView(R.id.payment)
    TextView payment;
    @BindView(R.id.task)
    TextView taskView;
    @BindView(R.id.manager)
    TextView manager;
    @BindView(R.id.space)
    View space;

    private GoogleMap googleMap;

    private Geocoder geocoder;

    private ArrayList<Task> tasks;
    private Marker searchMarker;
    @Nullable
    private Marker taskMarker;
    private int position;

    private boolean failedToFind = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        layout.requestFocus();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ((ViewGroup.MarginLayoutParams) card.getLayoutParams()).topMargin =
                    ViewUtil.getStatusBarHeight(getApplicationContext()) + ViewUtil.dp2px(16);
        }

        geocoder = new Geocoder(getApplicationContext());

        tasks = new ArrayList<>();
        tasks.addAll(getIntent().getParcelableArrayListExtra(Constants.EXTRA_TASKS));
        position = getIntent().getIntExtra(Constants.EXTRA_POSITION, 0);

        ((MyMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
            .getMapAsync(this);

        setTaskDescription(tasks.get(position));
        onExpand();

        requireService();
    }

    @Override
    public void onTasks(ArrayList<Task> tasks) {
        this.tasks.clear();
        this.tasks.addAll(tasks);
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < this.tasks.size(); j++) {
                if (this.tasks.get(j).achieved) {
                    continue;
                }
                if (this.tasks.get(j).lat == null || this.tasks.get(j).lng == null) {
                    this.tasks.get(j).achieved = false;
                    continue;
                }
                this.tasks.get(j).achieved = Math.abs(this.tasks.get(j).lat -
                    coordinates.get(i).lat) < 0.001 && Math.abs(this.tasks.get(j).lng -
                    coordinates.get(i).lng) < 0.001;
            }
        }
        if (position < tasks.size()) {
            setTaskDescription(tasks.get(position));
        } else {
            setTaskDescription(new Task(true));
        }
        notifyMarkers(false);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onCoordinatesEvent(CoordinatesEvent event) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).lat == null || tasks.get(i).lng == null) {
                continue;
            }
            if (Math.abs(tasks.get(i).lat - event.lat) < 0.001 &&
                Math.abs(tasks.get(i).lng - event.lng) < 0.001) {
                showNotification(i + 1);
                return;
            }
        }
    }

    private void requireService() {
        if (!CoordinatesService.isRunning(getApplicationContext())) {
            startService(CoordinatesService.getStartIntent(getApplicationContext()));
        }
        LocationManager locationManager = new LocationManager.Builder(getApplicationContext())
            .activity(this)
            .configuration(LocationUtil.getConfiguration())
            .build();
        locationManager.get();
    }

    @OnClick(R.id.observe)
    void onObserve() {
        if (failedToFind) {
            showSnackbar(R.string.error_task);
        }
        googleMap.animateCamera(CameraUpdateFactory
            .newLatLngZoom(new LatLng(55.752220, 37.615560), 9.0f));
    }

    @OnEditorAction(R.id.input)
    boolean onEnter() {
        ViewUtil.hideKeyboard(getApplicationContext());
        String address = addressView.getText().toString();
        if (searchMarker != null) {
            searchMarker.remove();
            searchMarker = null;
        }
        if (!address.equals(tasks.get(position).address)) {
            LatLng latLng = LocationUtil.createLatLng(new Task(address), geocoder, false);
            if (latLng != null) {
                failedToFind = false;
                searchMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng));
                googleMap.animateCamera(CameraUpdateFactory
                    .newLatLngZoom(latLng, 16.0f));
            } else {
                showSnackbar(R.string.error_search);
            }
        } else if (taskMarker != null) {
            googleMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(taskMarker.getPosition(), 16.0f));
        } else {
            showSnackbar(R.string.error_task);
        }
        return true;
    }

    @OnClick(R.id.back)
    void onBack() {
        finish();
    }

    private void setTaskDescription(Task task) {
        addressView.setText(task.address);
        index.setText(getString(R.string.caption_index, position + 1, ""));
        name.setText(getString(R.string.caption_name, task.name));
        address.setText(getString(R.string.caption_address, task.address));
        phone.setText(getString(R.string.caption_phone, task.phone));
        contactName.setText(getString(R.string.caption_contactName, task.contactName));
        payment.setText(getString(R.string.caption_payment, task.payment));
        taskView.setText(getString(R.string.caption_task, task.task));
        manager.setText(getString(R.string.caption_manager, task.manager));
    }

    @OnClick(R.id.expand)
    void onExpand() {
        if (expand.getTag() == null) {
            expand.setTag(true);
        }
        boolean visible = !((boolean) expand.getTag());
        expand.setTag(visible);
        expand.setImageResource(visible ? R.drawable.ic_keyboard_arrow_up_24dp :
            R.drawable.ic_keyboard_arrow_down_24dp);
        ViewUtil.changeVisibility(visible, index, name, address, phone, contactName, payment,
            taskView, manager, space);
    }

    @Override
    @SuppressWarnings("all")
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) &&
            hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            googleMap.setMyLocationEnabled(true);
        }
        googleMap.setOnMarkerClickListener((Marker marker) -> {
            googleMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(marker.getPosition(), 16.0f));
            return true;
        });
        googleMap.setOnMyLocationButtonClickListener(() -> {
            requireService();
            return false;
        });
        notifyMarkers(true);
    }

    @SuppressWarnings("all")
    private void notifyMarkers(boolean showMessage) {
        googleMap.clear();
        Observable.fromCallable(() -> {
            IconGenerator iconGenerator = new IconGenerator(getApplicationContext());
            ArrayList<MarkerOptions> options = new ArrayList<>();
            for (int i = 0; i < tasks.size(); i++) {
                options.add(LocationUtil.createMarkerOptions(iconGenerator,
                    LocationUtil.createLatLng(tasks.get(i), geocoder, true),
                    String.format("№%d", i + 1), i == position ?
                        getString(R.color.colorPrimary) : (tasks.get(i).achieved ?
                        getString(R.color.colorAccent) : null)));
            }
            return options;
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<ArrayList<MarkerOptions>>() {
                @Override
                public void onNext(ArrayList<MarkerOptions> response) {
                    for (int i = 0; i < response.size(); i++) {
                        if (response.get(i) != null) {
                            if (i == position) {
                                taskMarker = MapsActivity.this.googleMap.addMarker(response.get(i));
                            } else {
                                MapsActivity.this.googleMap.addMarker(response.get(i));
                            }
                        } else {
                            failedToFind = true;
                        }
                    }
                    if (showMessage && failedToFind) {
                        showSnackbar(R.string.error_task);
                    }
                }
            });
    }

    private void showSnackbar(@StringRes int id) {
        Snackbar.make(layout, id, Snackbar.LENGTH_LONG)
            .show();
    }
}
