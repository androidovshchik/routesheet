package rf.androidovshchik.routesheet.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import timber.log.Timber;

public final class ViewUtil {

    @SuppressWarnings("unused")
    public static int dp2px(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    @SuppressWarnings("all")
    public static Point getWindow(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @SuppressWarnings("all")
    public static Point getScreen(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            display.getRealSize(size);
        } else {
            Method methodGetRawHeight, methodGetRawWidth;
            try {
                methodGetRawHeight = Display.class.getMethod("getRawHeight");
                methodGetRawWidth = Display.class.getMethod("getRawWidth");
                size.y = (Integer) methodGetRawHeight.invoke(display);
                size.x = (Integer) methodGetRawWidth.invoke(display);
            } catch (NoSuchMethodException e) {
                Timber.e(e.getMessage());
            } catch (IllegalAccessException e) {
                Timber.e(e.getMessage());
            } catch (InvocationTargetException e) {
                Timber.e(e.getMessage());
            }
        }
        return size;
    }

    @SuppressWarnings("unused")
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @SuppressWarnings("unused")
    public static int getNavigationBarHeight(Context context) {
        Resources resources = context.getResources();
        boolean portraitOrientation = resources.getConfiguration().orientation ==
            Configuration.ORIENTATION_PORTRAIT;
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        int resourceId = resources.getIdentifier(portraitOrientation ? "navigation_bar_height" :
            "navigation_bar_height_landscape", "dimen", "android");
        if (resourceId > 0 && !hasMenuKey) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    @SuppressWarnings("all")
    public static void hideKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    @SuppressWarnings("all")
    public static void changeVisibility(boolean visible, View... views) {
        for (View view : views) {
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }
}
