package rf.androidovshchik.routesheet.remote;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;
import rf.androidovshchik.routesheet.models.Response;
import rf.androidovshchik.routesheet.models.Task;
import timber.log.Timber;

public class Client {

    private static final String TASKS = "tasks";

    public final Api api;

    private final Map<String, AsyncSubject<?>> subjects;

    public Client(Api api) {
        this.api = api;
        this.subjects = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public AsyncSubject<Response<Task>> fetchTasks(boolean refresh, String imei) {
        if (refresh || !subjects.containsKey(TASKS)) {
            subjects.remove(TASKS);
            subjects.put(TASKS, AsyncSubject.create());
            api.getTasks(imei)
                .subscribeOn(Schedulers.io())
                .subscribe((AsyncSubject<Response<Task>>) subjects.get(TASKS));
        }
        return (AsyncSubject<Response<Task>>) subjects.get(TASKS);
    }
}