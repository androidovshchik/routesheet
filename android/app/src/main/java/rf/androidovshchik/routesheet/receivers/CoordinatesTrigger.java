package rf.androidovshchik.routesheet.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.routesheet.Constants;
import rf.androidovshchik.routesheet.services.CoordinatesService;
import timber.log.Timber;

public class CoordinatesTrigger extends BroadcastReceiver {

	@Override
	@SuppressWarnings("ConstantConditions")
	public void onReceive(Context context, Intent intent) {
		Timber.d(intent.getAction());
		Intent service = CoordinatesService.getStartIntent(context);
		if (CoordinatesService.isRunning(context)) {
			context.stopService(service);
		}
		switch (intent.getAction()) {
			case Constants.ACTION_STOP:
				Intent receiver = new Intent(context, CoordinatesTrigger.class);
				receiver.setAction(Constants.ACTION_RESTART);
				PendingIntent pendingReceiver = PendingIntent.getBroadcast(context,
					0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);
				((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.cancel(pendingReceiver);
				break;
			case Constants.ACTION_RESTART:
				context.startService(service);
				break;
		}
	}
}
