package rf.androidovshchik.routesheet.models;

import com.google.gson.annotations.SerializedName;

public class Coordinates {

    @SerializedName("lat")
    public float lat;

    @SerializedName("lng")
    public float lng;
}