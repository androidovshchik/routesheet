package rf.androidovshchik.routesheet;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yayandroid.locationmanager.LocationManager;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rf.androidovshchik.routesheet.data.Prefs;
import rf.androidovshchik.routesheet.remote.Api;
import rf.androidovshchik.routesheet.remote.Client;
import timber.log.Timber;

public class RouteSheet extends Application {

	public static Client client;

	@SuppressWarnings("all")
	public static void sendRegistrationIdToBackend(Context context) {
		FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
		String token = instanceID.getToken();
		if (token == null || token.trim().isEmpty()) {
			return;
		}
		Timber.d("sendRegistrationIdToBackend");
		if (context.checkCallingOrSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
			== PackageManager.PERMISSION_GRANTED) {
			TelephonyManager telephonyManager =
				(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			client.api.setGCMToken(telephonyManager.getDeviceId(), token)
				.subscribeOn(Schedulers.io())
				.subscribe((response) -> {}, throwable -> Timber.e(throwable));
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Timber.plant(new Timber.DebugTree());
		Prefs prefs = new Prefs(getApplicationContext());
		prefs.printAll();
		if (BuildConfig.DEBUG) {
			LocationManager.enableLog(true);
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectAll()
				.penaltyDialog()
				.penaltyLog()
				.permitDiskReads()
				.build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectAll()
				.penaltyLog()
				.build());
		}
		Gson gson = new GsonBuilder()
			.create();
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.addInterceptor(logging);
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(Api.ENDPOINT)
			.addConverterFactory(GsonConverterFactory.create(gson))
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.client(httpClient.build())
			.build();
		client = new Client(retrofit.create(Api.class));
	}
}