package rf.androidovshchik.routesheet.ui.main;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rf.androidovshchik.routesheet.utils.ViewUtil;

public class TasksDecoration extends RecyclerView.ItemDecoration {

    private static final int SPACE = ViewUtil.dp2px(16);

    public TasksDecoration() {}

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int itemsCount = parent.getAdapter().getItemCount();
        if (position == itemsCount - 1) {
            outRect.bottom = SPACE;
        } else {
            outRect.bottom = 0;
        }
        outRect.top = SPACE;
        outRect.left = SPACE;
        outRect.right = SPACE;
    }
}