package rf.androidovshchik.routesheet.ui.base;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.RouteSheet;
import rf.androidovshchik.routesheet.models.Coordinates;
import rf.androidovshchik.routesheet.models.Response;
import rf.androidovshchik.routesheet.models.Task;
import rf.androidovshchik.routesheet.remote.Subscriber;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

    protected CompositeDisposable disposable = new CompositeDisposable();

    private ScheduledFuture<?> tasksChecker;

    protected TelephonyManager telephonyManager;

    protected ArrayList<Coordinates> coordinates;

    private boolean checkCoordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RouteSheet.sendRegistrationIdToBackend(getApplicationContext());
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        coordinates = new ArrayList<>();
    }

    @Override
    @SuppressWarnings("all")
    public void onStart() {
        super.onStart();
        checkCoordinates = true;
        EventBus.getDefault().register(this);
        ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
        tasksChecker = timer.scheduleWithFixedDelay(() -> {
            Timber.d("Checking tasks...");
            if (hasPermission(Manifest.permission.READ_PHONE_STATE)) {
                checkCoordinates = !checkCoordinates;
                if (checkCoordinates) {
                    Timber.d("Checking coordinates...");
                    disposable.clear();
                    disposable.add(RouteSheet.client.api
                        .getCoordinates(telephonyManager.getDeviceId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new Subscriber<Response<Coordinates>>() {
                            @Override
                            public void onNext(Response<Coordinates> response) {
                                if (response.isSuccessful()) {
                                    coordinates.clear();
                                    coordinates.addAll(response.response);
                                }
                            }
                        }));
                }
                disposable.add(RouteSheet.client.fetchTasks(true,
                    telephonyManager.getDeviceId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new Subscriber<Response<Task>>() {
                        @Override
                        public void onNext(Response<Task> response) {
                            if (response.isSuccessful()) {
                                onTasks(response.response);
                            }
                        }
                    }));
            }
        }, 0, 5, TimeUnit.SECONDS);
    }

    public abstract void onTasks(ArrayList<Task> tasks);

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (tasksChecker != null) {
            tasksChecker.cancel(true);
        }
    }

    @SuppressWarnings("all")
    protected void showNotification(int task) {
        NotificationManager notificationManager = (NotificationManager)
            getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
                getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
            getString(R.string.app_name))
            .setSmallIcon(R.drawable.ic_directions_car_white_24dp)
            .setContentText(getString(R.string.title_nearby, task))
            .setSound(soundUri)
            .setAutoCancel(true);
        notificationManager.notify(11, builder.build());
    }

    @SuppressWarnings("unused")
    protected void showMessage(@StringRes int id) {
        showMessage(getString(id));
    }

    protected void showMessage(@Nullable String message) {
        if (message == null) {
            return;
        }
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
                .show();
    }

    protected boolean hasPermission(String permission) {
        return checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
