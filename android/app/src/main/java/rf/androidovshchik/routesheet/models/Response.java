package rf.androidovshchik.routesheet.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.reactivex.annotations.Nullable;

public class Response<T> {

    private static final int ERROR = -1;
    private static final int SUCCESS = 0;
    private static final int WARNING = 1;

    @SerializedName("status")
    private int status;

    @Nullable
    @SerializedName("message")
    public String message;

    @Nullable
    @SerializedName("response")
    public ArrayList<T> response;

    public boolean isSuccessful() {
        return status == SUCCESS;
    }
}