package rf.androidovshchik.routesheet.ui.main;

import android.Manifest;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rf.androidovshchik.routesheet.R;
import rf.androidovshchik.routesheet.RouteSheet;
import rf.androidovshchik.routesheet.events.CoordinatesEvent;
import rf.androidovshchik.routesheet.events.NotificationEvent;
import rf.androidovshchik.routesheet.models.Response;
import rf.androidovshchik.routesheet.models.Task;
import rf.androidovshchik.routesheet.remote.Subscriber;
import rf.androidovshchik.routesheet.ui.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private TasksAdapter adapter;

    private RxPermissions rxPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(R.string.title_main);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new TasksDecoration());
        adapter = new TasksAdapter();
        recyclerView.setAdapter(adapter);

        rxPermissions = new RxPermissions(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onNotificationEvent(NotificationEvent event) {
        refresh(true);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onCoordinatesEvent(CoordinatesEvent event) {
        for (int i = 0; i < adapter.items.size(); i++) {
            if (adapter.items.get(i).lat == null || adapter.items.get(i).lng == null) {
                continue;
            }
            if (Math.abs(adapter.items.get(i).lat - event.lat) < 0.001 &&
                Math.abs(adapter.items.get(i).lng - event.lng) < 0.001) {
                showNotification(i + 1);
                return;
            }
        }
    }

    @Override
    public void onTasks(ArrayList<Task> tasks) {
        adapter.items.clear();
        adapter.items.addAll(tasks);
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < adapter.items.size(); j++) {
                if (adapter.items.get(j).achieved) {
                    continue;
                }
                if (adapter.items.get(j).lat == null || adapter.items.get(j).lng == null) {
                    adapter.items.get(j).achieved = false;
                    continue;
                }
                adapter.items.get(j).achieved = Math.abs(adapter.items.get(j).lat -
                    coordinates.get(i).lat) < 0.001 && Math.abs(adapter.items.get(j).lng -
                    coordinates.get(i).lng) < 0.001;
            }
        }
        adapter.notifyDataSetChanged();
    }

    @SuppressWarnings("all")
    public void refresh(boolean force) {
        if (force) {
            disposable.clear();
        }
        rxPermissions
            .request(Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
            .subscribe(granted -> {
                if (granted) {
                    disposable.add(RouteSheet.client.fetchTasks(force,
                        telephonyManager.getDeviceId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new Subscriber<Response<Task>>() {
                            @Override
                            public void onNext(Response<Task> response) {
                                showMessage(response.message);
                                if (response.isSuccessful()) {
                                    onTasks(response.response);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                super.onError(e);
                                showMessage(R.string.error_request);
                            }
                        }));
                }
            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
