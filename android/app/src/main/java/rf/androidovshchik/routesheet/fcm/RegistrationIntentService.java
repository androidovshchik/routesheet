package rf.androidovshchik.routesheet.fcm;

import android.app.IntentService;
import android.content.Intent;

import rf.androidovshchik.routesheet.RouteSheet;

public class RegistrationIntentService extends IntentService {

	private static final String TAG = "RegistrationIntentService";

	public RegistrationIntentService() {
		super(TAG);
	}

	@Override
	@SuppressWarnings("all")
	protected void onHandleIntent(Intent intent) {
		RouteSheet.sendRegistrationIdToBackend(getApplicationContext());
	}
}