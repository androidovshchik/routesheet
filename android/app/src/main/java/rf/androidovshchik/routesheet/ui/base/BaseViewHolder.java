package rf.androidovshchik.routesheet.ui.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @SuppressWarnings("unused")
    public Context getApplicationContext() {
        return itemView.getContext().getApplicationContext();
    }

    @SuppressWarnings("unused")
    public String getString(@StringRes int id, Object... params) {
        return itemView.getContext().getString(id, params);
    }
}