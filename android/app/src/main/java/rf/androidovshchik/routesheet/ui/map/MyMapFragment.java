package rf.androidovshchik.routesheet.ui.map;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.SupportMapFragment;

import rf.androidovshchik.routesheet.utils.ViewUtil;

public class MyMapFragment extends SupportMapFragment {

	@Override
	@SuppressWarnings("all")
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View mapView = super.onCreateView(inflater, container, savedInstanceState);
		View locationButton = ((View) mapView.findViewById(1).getParent()).findViewById(2);
		RelativeLayout.LayoutParams params =
			(RelativeLayout.LayoutParams) locationButton.getLayoutParams();
		params.height = ViewUtil.dp2px(56);
		params.width = ViewUtil.dp2px(56);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		int bottomMargin = ViewUtil.dp2px(16);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			bottomMargin += ViewUtil.getNavigationBarHeight(getContext().getApplicationContext());
		}
		params.setMargins(0, 0, ViewUtil.dp2px(16), bottomMargin);
		return mapView;
	}
}